urbdict_bot is a simple Telegram bot that searches for definitions on urbandictionary.com. Since none of the existing bots worked anymore, I made another one.

[![Pipeline Status][pipeline-badge]][pipeline-url]
[![Pylint][pylint-badge]][pylint-url]
[![coverage report][cover-badge]][cover-url]
[![Codestyle: black][codestyle-shield]][codestyle-url]
[![python version][python-version-shield]][python-version-url]
[![Python-Telegram-Bot][ptb-shield]][ptb-url]

# Table of Contents
* [About urbdict](#about)
  - [Build with](#buildwith)
* [Getting Started](#start)
  - [Installation](#installation)
  - [Configuration](#config)
  - [Running the bot](#running)
* [Usage](#usage)
* [Contributing](#contrib)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

<a name="about"></a>
## About urbdict
Yet another Urban Dictionary Telegram Bot. After all the others stopped working,
I made my own.

<a name="buildwith"></a>
### Build with
urbdict uses [Python-Telegram-Bot][ptb-url], *'a wrapper you can't refuse'*.

<a name="start"></a>
## Getting Started
To get the bot up and running follow these simple steps.

<a name="installation"></a>
### Installation
First clone the repository:
```sh
git clone https://gitlab.com/chgaida/urbdict-bot.git
```
Install from `requirements.txt`:
```sh
pip install -r requirements.txt
```

<a name="config"></a>
### Configuration
Make a copy of `config.sample.yaml` into the /src folder and fill in the empty fields:
```
cp config.sample.yaml src/config.yaml
```

You need to add at least your Telegram bot token to the config file for the bot to run.


<a name="running"></a>
### Running the bot
To start the bot in polling mode type:
```sh
./urbdict.py
```
If you want to use webhooks, use the following command line option:
```sh
./urbdict.py webhooks
```
In this case you have to specify your server IP and the paths to your server
certificate and private key in the configuration file.


<a name="usage"></a>
## Usage
There are only a few simple commands:

### In groups
* `/dict [term]` - Urban Dictionary querry
* `/rand` - shows a random entry
* `/help` - a quick help
* `/gdpr` - displays the privacy policy

### In private chat with the bot
* Just type in a search term.

<a name="contrib"></a>
## Contributing
Please take a look at [CONTRIBUTING.md](./CONTRIBUTING.md) if you're interested in helping!

<a name="license"></a>
## License
Distributed under the MIT License. See [LICENSE](.LICENSE) for more information.
<a name="contact"></a>
## Contact
telegram [@justplants_dev](https://t.me/justplants_dev)

finger(1) finger@port70.de


<a name="acknowledgements"></a>
## Acknowledgements





<!-- MARKDOWN LINKS & IMAGES -->
[pipeline-badge]: https://gitlab.com/chgaida/urbdict-bot/badges/develop/pipeline.svg
[pipeline-url]: https://gitlab.com/chgaida/urbdict-bot/-/commits/develop
[pylint-badge]: https://gitlab.com/chgaida/urbdict-bot/-/jobs/artifacts/develop/raw/public/badges/pylint.svg?job=pylint
[pylint-url]: https://gitlab.com/chgaida/urbdict-bot/-/commits/develop
[cover-badge]: https://gitlab.com/chgaida/urbdict-bot/badges/develop/coverage.svg
[cover-url]: https://gitlab.com/chgaida/urbdict-bot/-/commits/develop
[codestyle-shield]: https://img.shields.io/badge/code%20style-black-000000.svg
[codestyle-url]: https://github.com/psf/black
[python-version-shield]: https://img.shields.io/badge/python-3.7-blue
[python-version-url]: https://www.python.org/downloads/release/python-370/
[ptb-shield]: https://img.shields.io/badge/Python--Telegram--Bot-V13.6-blue
[ptb-url]: https://python-telegram-bot.org/

