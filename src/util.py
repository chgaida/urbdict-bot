"""Utility functions."""


import sys
import logging
from logging import handlers
from functools import wraps
import bleach
from telegram.utils.helpers import effective_message_type


def init_logging():
    """Create and initialize a logger."""
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    frmt = logging.Formatter(
        "[%(levelname)s] %(asctime)s |%(funcName)18s | %(message)s", "%m-%d %H:%M:%S"
    )

    # Make sure that there are not more than one handlers
    if not logger.handlers:
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(frmt)
        logger.addHandler(console_handler)

        file_handler = handlers.TimedRotatingFileHandler(
            "ud.log", when="d", interval=1, backupCount=3
        )
        file_handler.setFormatter(frmt)
        logger.addHandler(file_handler)

    return logger


def strip_html(text):
    """Strips all other html tags except allowed ones."""
    tags = ["b", "em", "a", "code", "u"]
    stripped = bleach.clean(text, tags=tags, strip=True)

    return stripped


def private_chat_only(func):
    """Wrapper for private only."""

    @wraps(func)
    def wrapped(update, context, *args, **kwargs):
        if update.effective_chat.type != "private":
            return func
        return func(update, context, *args, **kwargs)

    return wrapped


def text_only(func):
    """Wrapper for selected message types."""

    @wraps(func)
    def wrapped(update, context, *args, **kwargs):
        if effective_message_type(update) != "text" or update.message.via_bot:
            return func
        return func(update, context, *args, **kwargs)

    return wrapped
