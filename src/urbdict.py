#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=W0613


"""
    urbdict_bot version 0.0.1

"""

import sys
from uuid import uuid4
import requests

from telegram import InlineQueryResultArticle, ParseMode, InputTextMessageContent
from telegram.ext import (
    Updater,
    InlineQueryHandler,
    Filters,
    CommandHandler,
    MessageHandler,
)
from util import init_logging, private_chat_only, text_only, strip_html
from config import TELEGRAM_BOT_TOKEN, BOT_VERSION, PUB_IP, CERT, PRIV_KEY


# Debug Mode default Off
debug = True  # pylint: disable=C0103

# Init logging
LOGGER = init_logging()

# Urban Dictionary API URL
URL = "http://api.urbandictionary.com/v0/"
PATH = "define"
RND_PATH = "random"
QUERRY_KEY = "term"


def start(update, context):
    """Send a welcome on /start."""
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Hi, just start typing, /help for a short info.",
    )


def successful_query(request):
    """Check the request."""
    if request.status_code == 200 and request.json()["list"]:
        return True

    return False


def footer(dict_entry):
    """Create a footer with up-/downvotes and permalink."""
    footer_msg = f'\n\n👍 {dict_entry["thumbs_up"]} 👎 {dict_entry["thumbs_down"]} \
            <a href="{dict_entry["permalink"]}">Permalink</a>'

    return footer_msg


def build_reply(response, search_term, entry_num=0):
    """Create a reply."""
    if not successful_query(response):
        msg = r"¯\_(ツ)_/¯"
        msg += f"\n\nSorry, I couldn't find: {search_term}"
    else:
        entries = response.json()
        entry = entries["list"][entry_num]

        msg = f"{entry['word']}\n\n"
        msg += f"Definition:\n{entry['definition']}\n\n"
        msg += f"Example:\n{entry['example']}"
        msg += footer(entry)

    return msg


def get_definition(search_term, entry_num=0):
    """Get the definition from urbandictionary.com."""
    if search_term:
        payload = {QUERRY_KEY: search_term}
        response = requests.get(URL + PATH, params=payload)
    else:
        response = requests.get(URL + RND_PATH)

    return build_reply(response, search_term, entry_num)


def answer_request(update, context, message):
    """Send message."""
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=message,
        parse_mode=ParseMode.HTML,
        disable_web_page_preview=True,
    )


@text_only
@private_chat_only
def process_message(update, context):
    """Process every new update (noncommands)."""
    text = update.effective_message.text
    msg = get_definition(text)
    answer_request(update, context, msg)


@text_only
def querry_dict(update, context):
    """Handle the /dict command."""
    text = " ".join(context.args)

    msg = get_definition(text)
    answer_request(update, context, msg)


def random_definition(update, context):
    """Fetch a random definition."""
    msg = get_definition("")
    answer_request(update, context, msg)


def print_help(update, context):
    """Outputs a brief help message."""
    help_msg = "Use /dict [term] to search for a definition or if you are in\
    private chat just type away."

    context.bot.send_message(chat_id=update.effective_chat.id, text=help_msg)


def print_gdpr(update, context):
    """Outputs a short privacy policy."""
    msg = "We do not store anything. Period."
    context.bot.send_message(chat_id=update.effective_chat.id, text=msg)


def inlinequery(update, context):
    """Handle the inline query."""
    query = update.inline_query.query

    if query == "":
        return

    results = list()

    payload = {QUERRY_KEY: query}
    response = requests.get(URL + PATH, params=payload)

    entries = response.json()
    entries = entries.get("list")

    for i, entry in enumerate(entries[0:5]):
        results.append(
            InlineQueryResultArticle(
                id=str(uuid4()),
                title=entry["definition"],
                input_message_content=InputTextMessageContent(
                    strip_html(get_definition(query, i)),
                    disable_web_page_preview=True,
                    parse_mode=ParseMode.HTML,
                ),
            )
        )

    update.inline_query.answer(results)


def error(update, context):
    """Log Errors caused by Updates."""
    LOGGER.warning('Update "%s" caused error "%s"', update, context.error)


def start_local(updater):
    """Start the bot on local machine."""
    updater.start_polling()  # (timeout=30)

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


def start_webhook(updater):
    """Start the bot on server."""
    updater.start_webhook(
        listen=PUB_IP,
        port=8443,
        url_path=TELEGRAM_BOT_TOKEN,
        key=PRIV_KEY,
        cert=CERT,
        webhook_url=f"https://{PUB_IP}:8443/{TELEGRAM_BOT_TOKEN}",
    )
    updater.idle()


def main():
    """Start the bot."""
    print(f"{BOT_VERSION[0], BOT_VERSION[1]} starting...")

    # Create EventHandler and pass it your bot's token.
    updater = Updater(token=TELEGRAM_BOT_TOKEN, use_context=True)
    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("dict", querry_dict))
    dispatcher.add_handler(CommandHandler("rand", random_definition))
    dispatcher.add_handler(CommandHandler("help", print_help))
    dispatcher.add_handler(CommandHandler("gdpr", print_gdpr))
    dispatcher.add_handler(InlineQueryHandler(inlinequery))
    dispatcher.add_handler(
        MessageHandler(Filters.all & ~Filters.command, process_message)
    )

    # log all errors
    dispatcher.add_error_handler(error)

    # Start polling or webhook
    if sys.argv[-1] == "webhook":
        start_webhook(updater)
    else:
        start_local(updater)


if __name__ == "__main__":
    main()
